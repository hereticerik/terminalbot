# TerminalBot


## Just another discord bot
A simple hybrid discord bot with customized features geared towards a couple of small comunnities. Nothing interesting.

## Installation
To be continued later when the project is installable.

## Upgrade
To be continued later when the project is upgradable. 

## Support
There is no support for this project, it is just a simple discord bot geared towards helping manage a couple of small communities.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
I don't mind if others contribute but it's not really needed, unless there is a serious security issue or huge bug.

## License
Use it however you want, it's meant for small communities that don't need much.
